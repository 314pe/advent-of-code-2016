package lt.petunov.adventcalender.day17;


import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Main {

    private static final String DOOR_IS_OPEN = "bcdef";
    private String passcode = "bwnlcvfs";
    private String shortestPasscode = null;
    private String longestPasscode = null;

    public static void main(String[] args) {

        Main m = new Main();
        m.findPath();

        System.out.println("Shortest path is " + m.getShortestPath());
        System.out.println("Longest path length is " + m.getLongestPathLength());

    }

    private void findPath() {
        getMove(passcode, 0, 0);
    }

    public String getShortestPath() {
        return shortestPasscode.substring(passcode.length());
    }

    public int getLongestPathLength() {
        return longestPasscode.substring(passcode.length()).length();
    }

    public void getMove(String passcode, int x, int y) {
        if (x == 3 && y == 3) {
            if (shortestPasscode == null || passcode.length() < shortestPasscode.length()) {
                shortestPasscode = passcode;
            }
            if (longestPasscode == null || passcode.length() > longestPasscode.length()) {
                longestPasscode = passcode;
            }
            return;
        }

        String result = getMd5(passcode).substring(0, 4);

        if (checkIfOpen(result.charAt(0)) && y > 0) {
            getMove(passcode + "U", x, y - 1);
        }
        if (checkIfOpen(result.charAt(1)) && y < 3) {
            getMove(passcode + "D", x, y + 1);
        }
        if (checkIfOpen(result.charAt(2)) && x > 0) {
            getMove(passcode + "L", x - 1, y);
        }
        if (checkIfOpen(result.charAt(3)) && x < 3) {
            getMove(passcode + "R", x + 1, y);
        }

    }

    private boolean checkIfOpen(char c) {
        return (DOOR_IS_OPEN).contains(c + "");
    }


    private String getMd5(String key) {
        MessageDigest md;
        String md5 = "";
        try {
            md = MessageDigest.getInstance("MD5");
            md5 = DatatypeConverter.printHexBinary(md.digest((key).getBytes())).toLowerCase();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return md5;
    }
}
