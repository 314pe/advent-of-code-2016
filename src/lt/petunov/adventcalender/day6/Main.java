package lt.petunov.adventcalender.day6;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class Main {

    public static void main(String[] args) {
        String[] signal = decodeSignal(readFile("input/day6/test.txt"));
        System.out.println("Testing");
        System.out.println("most likely it's " + signal[0] + " least likely it's " + signal[1]);

        signal = decodeSignal(readFile("input/day6/input.txt"));
        System.out.println("Input");
        System.out.println("most likely it's " + signal[0] + " least likely it's " + signal[1]);

    }

    private static String[] decodeSignal(List<String> signals) {
        StringBuffer mostLikely = new StringBuffer();
        StringBuffer leastLikely = new StringBuffer();

        ArrayList<HashMap<String, Integer>> codes = new ArrayList<>();

        for (int i = 0; i < signals.get(0).length(); i++) {
            codes.add(new HashMap<String, Integer>());
        }

        for (String signal : signals) {
            String[] letters = signal.split("");
            for (int i = 1; i < letters.length; i++) {
                String letter = letters[i];

                if (letter.equals(""))
                    continue;

                HashMap<String, Integer> code = codes.get(i - 1);
                if (code.containsKey(letter)) {
                    code.put(letter, code.get(letter) + 1);
                } else {
                    code.put(letter, 1);
                }
            }
        }

        for (int i = 0; i < codes.size(); i++) {
            HashMap<String, Integer> code = codes.get(i);
            Entry<String, Integer> maxEntry = null;
            Entry<String, Integer> minEntry = null;
            for (Entry<String, Integer> entry : code.entrySet()) {
                if (maxEntry == null || entry.getValue() > maxEntry.getValue()) {
                    maxEntry = entry;
                }
                if (minEntry == null || entry.getValue() < minEntry.getValue()) {
                    minEntry = entry;
                }
            }
            mostLikely.append(maxEntry.getKey());
            leastLikely.append(minEntry.getKey());
        }

        return new String[] { mostLikely.toString(), leastLikely.toString() };
    }

    private static List<String> readFile(String path) {
        try {
            List<String> lines = Files.readAllLines(Paths.get(path), Charset.defaultCharset());
            return lines;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
