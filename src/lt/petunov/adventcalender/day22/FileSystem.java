package lt.petunov.adventcalender.day22;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lt.petunov.adventcalender.utils.Utils;

public class FileSystem {

    class Node implements Comparable<Node> {
        int x;
        int y;
        int size;
        int used;

        public int getAvailable() {
            return size - used;
        }

        @Override
        public int compareTo(Node o) {
            if (o != this)
                return Integer.compare(o.getAvailable(), getAvailable());
            return 0;
        }

    }

    private List<Node> nodes = new ArrayList<>();

    public FileSystem(List<String> readFile) {
        for (int i = 2; i < readFile.size(); i++) {
            nodes.add(getNode(readFile.get(i)));
        }
    }

    private Node getNode(String node) {
        Node result = new Node();

        Pattern pattern = Pattern.compile("(\\d+)-y(\\d+)\\s+(\\d+)T\\s+(\\d+)T");
        Matcher matcher = pattern.matcher(node);
        matcher.find();

        result.x = Integer.parseInt(matcher.group(1));
        result.y = Integer.parseInt(matcher.group(2));
        result.size = Integer.parseInt(matcher.group(3));
        result.used = Integer.parseInt(matcher.group(4));

        return result;
    }

    public static void main(String[] args) {
        FileSystem fs = new FileSystem(Utils.readFile("input/day22/input.txt"));

        System.out.println("Part 1 " + fs.countViableNodes());

        System.out.println("Part 2 " + fs.countPathSteps());

    }

    private int countPathSteps() {
        int sizeX = 0;
        int sizeY = 0;
        for (Node node : nodes) {
            if (node.x > sizeX)
                sizeX = node.x;
            if (node.y > sizeY)
                sizeY = node.y;
        }

        Node[][] array = new Node[sizeX + 1][sizeY + 1];
        for (Node node : nodes) {
            array[node.x][node.y] = node;
        }

        Node emptyNode = null;
        Node turnNode = null;

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (array[i][j].used == 0) {
                    emptyNode = array[i][j];
                } else if (array[i][j].size > 100) {
                    if (turnNode == null)
                        turnNode = array[i - 1][j];
                }
            }
        }


        int result = Math.abs(emptyNode.x - turnNode.x) + Math.abs(emptyNode.y - turnNode.y);
        result += Math.abs(turnNode.x - sizeX) + turnNode.y;
        return result + 5 * (sizeX - 1);
    }

    private int countViableNodes() {
        int result = 0;

        Collections.sort(nodes);

        for (Node node : nodes) {
            for (Node n : nodes) {
                // Same or empty node
                if (node.size == 0 || n == node)
                    continue;

                // The other nodes will be too small to fit the data.
                if (n.getAvailable() < node.size) {
                    break;
                } else {
                    result++;
                }
            }
        }

        return result;
    }

}
