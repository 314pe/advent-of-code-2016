package lt.petunov.adventcalender.day2part2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Main {

    static int[][] keypad = new int[5][5];

    static int i = 2;
    static int j = 0;

    public static void main(String[] args) {

        keypad[0][2] = 1;

        keypad[1][1] = 2;
        keypad[1][2] = 3;
        keypad[1][3] = 4;

        keypad[2][0] = 5;
        keypad[2][1] = 6;
        keypad[2][2] = 7;
        keypad[2][3] = 8;
        keypad[2][4] = 9;

        keypad[3][1] = 0xA;
        keypad[3][2] = 0xB;
        keypad[3][3] = 0xC;

        keypad[4][2] = 0xD;

        readFile("input/day2/test1.txt");
        readFile("input/day2/input.txt");
    }

    private static void readFile(String path) {
        System.out.print("Code is ");
        try {
            List<String> lines = Files.readAllLines(Paths.get(path));
            for (String line : lines) {
                System.out.print(Integer.toHexString(countCode(line)));
            }
            System.out.println();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static int countCode(String line) {
        String[] commands = line.split("");

        for (String command : commands) {
            switch (command) {
                case "U":
                    if (i > 0 && keypad[i - 1][j] != 0)
                        i--;
                    break;
                case "D":
                    if (i < 4 && keypad[i + 1][j] != 0)
                        i++;
                    break;
                case "L":
                    if (j > 0 && keypad[i][j - 1] != 0)
                        j--;
                    break;
                case "R":
                    if (j < 4 && keypad[i][j + 1] != 0)
                        j++;
                    break;
            }
        }

        return keypad[i][j];
    }

}
