package lt.petunov.adventcalender.day15;


import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lt.petunov.adventcalender.utils.Utils;

public class Main {

    class Disc {
        int positions = 0;
        int initialPosition = 0;

        public Disc(int positions, int initialPosition) {
            this.positions = positions;
            this.initialPosition = initialPosition;
        }

        public boolean fallsThrough(int time) {
            return (time + initialPosition) % positions == 0;
        }
    }

    List<Disc> discs = new ArrayList<>();

    public static void main(String[] args) {
        Main main = new Main();
        main.loadDiscs(Utils.readFile("input/day15/input.txt"));
        System.out.println("Wait " + main.findTime() + " seconds");
        main.loadDisc("Disc #7 has 11 positions; at time=0, it is at position 0.");
        System.out.println("Wait for second part " + main.findTime() + " seconds");
    }

    private int findTime() {
        int time = 0;
        boolean allFellTrough = false;
        while (!allFellTrough) {

            int tryTime = time;
            allFellTrough = true;
            for (Disc disc : discs) {
                tryTime++;

                if (!disc.fallsThrough(tryTime)) {
                    allFellTrough = false;
                    break;
                }
            }

            time++;
        }


        return time - 1;
    }

    private void loadDisc(String instruction) {
        Pattern pattern = Pattern.compile("Disc #(\\d+) has (\\d+) positions; at time=0, it is at position (\\d+)");
        Matcher matcher = pattern.matcher(instruction);

        matcher.find();

        Integer index = Integer.parseInt(matcher.group(1)) - 1;
        Integer positions = Integer.parseInt(matcher.group(2));
        Integer initialPosition = Integer.parseInt(matcher.group(3));

        discs.add(index, new Disc(positions, initialPosition));

    }

    private void loadDiscs(List<String> instructions) {
        for (String instruction : instructions) {
            loadDisc(instruction);
        }
    }


}
