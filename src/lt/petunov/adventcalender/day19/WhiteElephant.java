package lt.petunov.adventcalender.day19;

public class WhiteElephant {

    private static int numberOfElves = 3012210;
    private int[] elves = new int[numberOfElves];

    public WhiteElephant() {
        // All elves start with 1 present
        for (int i = 0; i < numberOfElves; i++) {
            elves[i] = 1;
        }
    }

    public static void main(String[] args) {
        new WhiteElephant().exchangeGifts();
    }

    private void exchangeGifts() {
        while (true) {
            for (int i = 0; i < numberOfElves; i++) {
                if (elves[i] > 0) {
                    steal(i);
                }

                if (elves[i] == numberOfElves) {
                    System.out.println((i + 1) + " has all the presents");
                    return;
                }
            }
        }
    }

    private void steal(int thief) {
        int victim = thief;

        // Find first elf with some presents
        do {
            victim = (victim + 1) % numberOfElves;
        } while (elves[victim] == 0);

        // Steal gifts
        elves[thief] += elves[victim];
        elves[victim] = 0;
    }
}
