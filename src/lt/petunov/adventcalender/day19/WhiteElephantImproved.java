package lt.petunov.adventcalender.day19;

import java.util.LinkedList;

public class WhiteElephantImproved {

    private static int numberOfElves = 3012210;
    private LinkedList<Integer> list1 = new LinkedList<>();
    private LinkedList<Integer> list2 = new LinkedList<>();

    public static void main(String[] args) {
        new WhiteElephantImproved().exchangeGifts();
    }

    private void exchangeGifts() {
        for (int i = 0; i <= numberOfElves; i++) {
            if (i <= numberOfElves / 2) {
                list1.addLast(i);
            } else {
                list2.addLast(i);
            }
        }

        while (list1.size() + list2.size() > 1) {
            int present = list1.pollFirst();
            // Finding middle element
            if (list1.size() == list2.size()) {
                list1.pollLast();
            } else {
                list2.pollFirst();
            }
            list2.addLast(present);

            // Redistribute presents among lists
            list1.addLast(list2.pollFirst());
        }

        System.out.println(list1.pollFirst() + " has all the presents.");

    }

}
