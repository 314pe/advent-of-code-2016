package lt.petunov.adventcalender.day1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

public class Main {

    public static class Point {
        public int x;
        public int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Point point = (Point) o;

            if (x != point.x) return false;
            return y == point.y;
        }

        @Override
        public int hashCode() {
            int result = x;
            result = 31 * result + y;
            return result;
        }
    }

    public enum Direction {
        NORTH(0, -1), SOUTH(0, 1), EAST(1, 0), WEST(-1, 0);
        int x, y;
        Direction left, right;

        Direction(int x, int y) {
            this.x = x;
            this.y = y;
        }

        static {
            NORTH.left = WEST;
            NORTH.right = EAST;
            SOUTH.left = EAST;
            SOUTH.right = WEST;
            EAST.left = NORTH;
            EAST.right = SOUTH;
            WEST.left = SOUTH;
            WEST.right = NORTH;
        }
    }

    public static void main(String[] args) {
        printAnswer("input/day1/test4.txt");
        printAnswer("input/day1/input.txt");
    }

    private static void printAnswer(String path) {
        try {
            String[] lines = Files.readAllLines(Paths.get(path)).get(0).split(", ");
            System.out.println("Distance " + calculateDistance(lines));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static int calculateDistance(String[] lines) {
        int x = 0;
        int y = 0;

        Direction direction = Direction.NORTH;

        Set<Point> places = new HashSet<>();


        for (String dir : lines) {

            if (dir.startsWith("R")) {
                direction = direction.right;
            } else {
                direction = direction.left;
            }

            for (int i = 0; i < Integer.parseInt(dir.substring(1)); i++) {
                x += direction.x;
                y += direction.y;

                Point place = new Point(x, y);

                if (places.contains(place)) {
                    System.out.println("Visited twice " + place.x + " " + place.y + " distance is " +
                            (Math.abs(place.x) + Math.abs(place.y)));
                }
                places.add(place);
            }


        }

        return Math.abs(x) + Math.abs(y);
    }
}
