package lt.petunov.adventcalender.day4;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
//        readFile("input/day4/test.txt");
        readFile("input/day4/input.txt");
    }

    private static void readFile(String path) {
        try {
            List<String> lines = Files.readAllLines(Paths.get(path), Charset.defaultCharset());
            Integer sumOfSectors = 0;
            Integer counterForRealRooms = 0;
            for (String line : lines) {
                String name = getName(line);
                int sector = getSector(line);
                String checkSum = getCheckSum(line);
                if (checkSum(name, checkSum)) {
                    System.out.println("" + decryptRoomName(name, sector) + " " + sector);
                    sumOfSectors += sector;
                    counterForRealRooms++;
                }
            }
            System.out.println("Sum of all sectors is " + +sumOfSectors);
            System.out.println("Total rooms " + counterForRealRooms);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String decryptRoomName(String name, int sector) {
        StringBuffer sb = new StringBuffer();

        String[] letters = name.split("");

        for (String letter : letters) {
            if (letter.equals(""))
                continue;
            else if (letter.equals("-")) {
                sb.append(letter);
            } else {
                sb.append(getShiftedValue(letter, sector));
            }
        }

        return sb.toString();
    }

    private static String getShiftedValue(String letter, int sector) {
        return "" + (char) ((letter.charAt(0) - 97 + sector) % 26 + 97);
    }

    private static boolean checkSum(String name, String checkSum) {
        class ValueComparator implements Comparator<String> {
            Map<String, Integer> base;

            public ValueComparator(Map<String, Integer> base) {
                this.base = base;
            }

            public int compare(String a, String b) {
                if (base.get(a) > base.get(b)) {
                    return -1;
                } else if (base.get(a) == base.get(b)) {
                    return a.charAt(0) > b.charAt(0) ? 1 : -1;
                } else {
                    return 1;
                }
            }
        }

        String[] letters = name.replaceAll("-", "").split("");

        HashMap<String, Integer> map = new HashMap<String, Integer>();

        for (String letter : letters) {
            if (letter.equals(""))
                continue;
            if (map.containsKey(letter)) {
                map.put(letter, map.get(letter) + 1);
            } else {
                map.put(letter, 1);
            }
        }

        ValueComparator vc = new ValueComparator(map);

        TreeMap<String, Integer> sorted_map = new TreeMap<String, Integer>(vc);
        sorted_map.putAll(map);
        String[] checkSumLetters = checkSum.split("");
        Iterator<String> nameLetters = sorted_map.keySet().iterator();

        // make it work with idx 0
        for (int i = 1; i < checkSumLetters.length; i++) {
            String letter = nameLetters.next();
            if (!checkSumLetters[i].equals(letter)) {
                return false;
            }
        }

        return true;
    }

    private static String getCheckSum(String line) {
        Pattern pattern = Pattern.compile("\\[(.+)\\]");
        Matcher m = pattern.matcher(line);
        m.find();
        return m.group(1);
    }

    private static int getSector(String line) {
        Pattern pattern = Pattern.compile("\\d+");
        Matcher m = pattern.matcher(line);
        m.find();
        return Integer.parseInt(m.group(0));
    }

    private static String getName(String line) {
        Pattern pattern = Pattern.compile("(.+)-\\d");

        Matcher m = pattern.matcher(line);
        m.find();

        return m.group(1);
    }


}
