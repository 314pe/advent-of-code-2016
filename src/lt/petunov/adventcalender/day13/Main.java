package lt.petunov.adventcalender.day13;

import java.awt.*;
import java.util.HashSet;
import java.util.Set;

public class Main {

    private static final int INPUT_NUMBER = 1352;

    private static final Point GOAL = new Point(31, 39);
    private static HashSet<Point> visited = new HashSet<>();


    public static void main(String[] args) {
        System.out.println("Shortest path is " + findShortestPath());
    }

    private static int findShortestPath() {
        Set<Point> start = new HashSet<>();
        start.add(new Point(1, 1));
        visited.add(new Point(1, 1));

        int steps = 0;
        while (!visited.contains(GOAL)) {

            steps++;
            start = addAdjacent(start);

            if (steps == 50)
                System.out.println("In 50 steps visited " + visited.size());

            if (start.isEmpty()) {
                return 0;
            }

        }


        return steps;
    }

    private static Set<Point> addAdjacent(Set<Point> start) {
        HashSet<Point> result = new HashSet<>();

        for (Point visitedPoint : start) {
            int x = visitedPoint.x;
            int y = visitedPoint.y;

            if (x != 0) {
                if (!visited.contains(new Point(x - 1, y)) && isClear(x - 1, y)) {
                    result.add(new Point(x - 1, y));
                    visited.add(new Point(x - 1, y));
                }
            }
            if (y != 0) {
                if (!visited.contains(new Point(x, y - 1)) && isClear(x, y - 1)) {
                    result.add(new Point(x, y - 1));
                    visited.add(new Point(x, y - 1));
                }
            }
            if (!visited.contains(new Point(x + 1, y)) && isClear(x + 1, y)) {
                result.add(new Point(x + 1, y));
                visited.add(new Point(x + 1, y));
            }
            if (!visited.contains(new Point(x, y + 1)) && isClear(x, y + 1)) {
                result.add(new Point(x, y + 1));
                visited.add(new Point(x, y + 1));
            }

        }

        return result;
    }

    private static boolean isClear(int x, int y) {
        int i = x * x + 3 * x + 2 * x * y + y + y * y + INPUT_NUMBER;

        return Integer.bitCount(i) % 2 == 0;
    }
}