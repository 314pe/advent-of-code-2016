package lt.petunov.adventcalender.day2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Main {

    static int[][] keypad = new int[3][3];

    static int i = 1;
    static int j = 1;

    public static void main(String[] args) {

        int k = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                keypad[i][j] = ++k;
            }
        }


        readFile("input/day2/test1.txt");
        readFile("input/day2/input.txt");
    }

    private static void readFile(String path) {
        System.out.print("Code is ");
        try {
            List<String> lines = Files.readAllLines(Paths.get(path));
            for (String line : lines) {
                System.out.print(countCode(line));
            }
            System.out.println();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static int countCode(String line) {
        String[] commands = line.split("");

        for (String command : commands) {
            switch (command) {
                case "U":
                    if (i > 0)
                        i--;
                    break;
                case "D":
                    if (i < 2)
                        i++;
                    break;
                case "L":
                    if (j > 0)
                        j--;
                    break;
                case "R":
                    if (j < 2)
                        j++;
                    break;
            }
        }

        return keypad[i][j];
    }

}
