package lt.petunov.adventcalender.day18;

public class Main {

    private String[] traps = { "^^.", ".^^", "^..", "..^" };
    private String currentRow = ".^^^.^.^^^.^.......^^.^^^^.^^^^..^^^^^.^.^^^..^^.^.^^..^.^..^^...^.^^.^^^...^^.^.^^^."
            + ".^^^^.....^....";
    private int rows;

    public Main(int rows) {
        super();
        this.rows = rows;
    }

    public static void main(String[] args) {
        System.out.println("Number of safe tiles " + new Main(40).calculateTiles());

        System.out.println("Number of safe tiles " + new Main(400000).calculateTiles());
    }

    private int calculateTiles() {
        int safeTiles = countSafe(currentRow);
        // System.out.println("safeTiles " + safeTiles);

        for (int i = 0; i < rows - 1; i++) {
            String[] split = currentRow.split("");
            
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < split.length - 1; j++) {
                sb.append(isSafe(j) ? "." : "^");
            }
            currentRow = sb.toString();
            safeTiles += countSafe(currentRow);
            // System.out.println(i + " safeTiles " + safeTiles);
        }

        return safeTiles;
    }

    private int countSafe(String s) {
        return s.length() - s.replace(".", "").length();
    }

    private boolean isSafe(int i) {
        String tiles = "";
        if (i == 0) {
            tiles = "." + currentRow.substring(i, 2);
        } else if (i == currentRow.length() - 1) {
            tiles = currentRow.substring(i - 1) + ".";
        } else {
            tiles = currentRow.substring(i - 1, i + 2);
        }

        for (String trap : traps) {
            if (tiles.equals(trap)) {
                return false;
            }
        }

        return true;
    }

}
