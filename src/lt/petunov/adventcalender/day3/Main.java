package lt.petunov.adventcalender.day3;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;


public class Main {

    public static void main(String[] args) {
        readFile("input/day3/input.txt");
    }

    private static void readFile(String path) {
        try {
            List<String> lines = Files.readAllLines(Paths.get(path));
            System.out.println("Counted horizontal " + countHorizontalTriangles(lines) + " triangles");
            System.out.println("Counted vertical " + countVerticalTriangles(lines) + " triangles");
            System.out.println("Total lines " + lines.size());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static int countVerticalTriangles(List<String> lines) {
        int result = 0;
        for (int i = 0; i < lines.size(); i += 3) {
            int[][] group = new int[3][3];
            group[0] = splitLine(lines.get(i));
            group[1] = splitLine(lines.get(i + 1));
            group[2] = splitLine(lines.get(i + 2));
            if (isTriangle(group[0][0], group[1][0], group[2][0]))
                result++;

            if (isTriangle(group[0][1], group[1][1], group[2][1]))
                result++;

            if (isTriangle(group[0][2], group[1][2], group[2][2]))
                result++;

        }
        return result;
    }

    private static int[] splitLine(String s) {
        int[] result = new int[3];
        String[] split = s.split("\\s+");

        result[0] = Integer.parseInt(split[1]);
        result[1] = Integer.parseInt(split[2]);
        result[2] = Integer.parseInt(split[3]);

        return result;
    }

    private static int countHorizontalTriangles(List<String> lines) {
        int result = 0;
        for (String line : lines) {
            if (isTriangle(line.split("\\s+")))
                result++;
        }
        return result;
    }

    private static boolean isTriangle(int a, int b, int c) {
        return (a + b > c) && (a + c > b) && (b + c > a);
    }

    private static boolean isTriangle(String[] split) {
        int a = Integer.parseInt(split[1]);
        int b = Integer.parseInt(split[2]);
        int c = Integer.parseInt(split[3]);

        return isTriangle(a, b, c);
    }

}
