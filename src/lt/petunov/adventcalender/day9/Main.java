package lt.petunov.adventcalender.day9;

import lt.petunov.adventcalender.utils.Utils;

public class Main {

    public static void main(String[] args) {
        System.out.println(
                "Length of a file is " + decompressFile(Utils.readFile("input/day9/input.txt").get(0)).length());

        System.out.println("V2 Length of a file is " + decompressFileV2(Utils.readFile("input/day9/input.txt").get(0)));

    }

    private static long decompressFileV2(String file) {
        long fileLength = 0;
        String[] characters = file.split("");

        int i = 1;
        while (i < characters.length) {
            if (characters[i].equals("(")) {
                StringBuffer sizeString = new StringBuffer();
                StringBuffer repeatString = new StringBuffer();

                i++;
                while (!characters[i].equals("x"))
                    sizeString.append(characters[i++]);
                int size = Integer.parseInt(sizeString.toString());

                i++;
                while (!characters[i].equals(")"))
                    repeatString.append(characters[i++]);
                int repeat = Integer.parseInt(repeatString.toString());

                fileLength += repeat * decompressFileV2(file.substring(i, i + size));
                i += size + 1;

                continue;
            } else {
                fileLength++;
            }

            i++;
        }

        return fileLength;
    }


    private static String decompressFile(String file) {
        StringBuffer sb = new StringBuffer();
        String[] characters = file.split("");

        int i = 0;
        while (i < characters.length) {
            if (characters[i].equals("(")) {
                StringBuffer sizeString = new StringBuffer();
                StringBuffer repeatString = new StringBuffer();

                i++;
                while (!characters[i].equals("x"))
                    sizeString.append(characters[i++]);
                int size = Integer.parseInt(sizeString.toString());

                i++;
                while (!characters[i].equals(")"))
                    repeatString.append(characters[i++]);
                int repeat = Integer.parseInt(repeatString.toString());
                
                for (int j = 0; j < repeat; j++) {
                    sb.append(file.substring(i, i + size));
                }
                i += size + 1;

                continue;
            } else {
                sb.append(characters[i]);
            }

            i++;
        }

        return sb.toString();
    }

}
