package lt.petunov.adventcalender.day14;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    private class Key {
        int idx = 0;
        String value = "";

        @Override
        public String toString() {
            return "Key{" +
                    "idx=" + idx +
                    ", value='" + value + '\'' +
                    '}';
        }
    }

    private static final String SALT = "jlmsuwbz";
//    private static final String SALT = "abc";

    private static final int EXPECTED_NUMBER_OF_KEYS = 64;

    private boolean stretch = false;


    public Main(boolean stretch) {
        this.stretch = stretch;
    }

    public static void main(String[] args) {
//        System.out.println("Index for 64th key " + new Main(false).getKeyIndex());
        System.out.println("Index for 64th stretched key " + new Main(true).getKeyIndex());
    }

    public int getKeyIndex() {
        List<Key> keys = new ArrayList<>();
        List<Integer> confirmedKeys = new ArrayList<>();

        int index = 0;

        while (confirmedKeys.size() < EXPECTED_NUMBER_OF_KEYS) {
            String keyString = getStretchedKey(index);

            Pattern pattern = Pattern.compile("((.)\\2\\2)");
            Matcher matcher = pattern.matcher(keyString);

            if (matcher.find()) {
                Key key = new Key();
                key.idx = index;
                key.value = new String(new char[5]).replace("\0", "" + matcher.group(1).charAt(0));

                keys.add(key);
            }

            for (Iterator<Key> iterator = keys.iterator(); iterator.hasNext(); ) {
                Key key = iterator.next();

                if (index > key.idx + 1000)
                    iterator.remove();

                if (key.idx == index)
                    continue;

                if (keyString.contains(key.value)) {
                    iterator.remove();
                    confirmedKeys.add(key.idx);

                    if (confirmedKeys.size() == EXPECTED_NUMBER_OF_KEYS)
                        break;

//                    System.out.println("confirmed " + confirmedKeys.size());
                }
            }

            index++;
        }

        Collections.sort(confirmedKeys);

        System.out.println("confirmed sorted keys " + confirmedKeys);

        return confirmedKeys.get(confirmedKeys.size() - 1);
    }

    private String getStretchedKey(int index) {
        String key = getKey(index);
        if (stretch) {
            for (int i = 0; i < 2016; i++) {
                key = getMd5(key);
            }
        }
        return key;
    }

    private String getMd5(String key) {
        MessageDigest md;
        String md5 = "";
        try {
            md = MessageDigest.getInstance("MD5");
            md5 = DatatypeConverter.printHexBinary(md.digest((key).getBytes())).toLowerCase();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return md5;
    }

    private String getKey(int index) {
        return getMd5(SALT + index);
    }
}
