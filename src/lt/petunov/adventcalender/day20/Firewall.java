package lt.petunov.adventcalender.day20;

import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import lt.petunov.adventcalender.utils.Utils;

public class Firewall {

    public static void main(String[] args) {
        List<String> input = Utils.readFile("input/day20/input.txt");

        TreeMap<Long, Long> ips = new TreeMap<>();

        for (String each : input) {
            String[] split = each.split("-");
            ips.put(Long.parseLong(split[0]), Long.parseLong(split[1]));
        }

        Iterator<Long> i = ips.navigableKeySet().iterator();
        Long first = i.next();
        Long max = 0L;
        Long totalFree = 0L;
        Long min = null;

        while (i.hasNext()) {
            Long current = i.next();
            if ((ips.get(first) + 1) > max)
                max = ips.get(first) + 1;
            if (current > max) {
                if (min == null)
                    min = max;
                
                totalFree += current - max;
            }
            first = current;
        }

        System.out.println("Min: " + min + " Count: " + totalFree);
    }

}
