package lt.petunov.adventcalender.day8;

import java.util.LinkedList;
import java.util.List;

import lt.petunov.adventcalender.utils.Utils;

public class Main {

    private static final int WIDTH = 50;
    private static final int HEIGHT = 6;
    // private static final int WIDTH = 7;
    // private static final int HEIGHT = 3;

    public static void main(String[] args) {
        // System.out.println("Pixels lit " + calculateLitPixels(Utils.readFile("input/day8/test.txt")));
        System.out.println("Pixels lit " + calculateLitPixels(Utils.readFile("input/day8/input.txt")));

    }

    private static int calculateLitPixels(List<String> commands) {
        LinkedList<LinkedList<Boolean>> screen = new LinkedList<>();

        for (int i = 0; i < HEIGHT; i++) {
            LinkedList<Boolean> line = new LinkedList<Boolean>();
            screen.add(line);
            for (int j = 0; j < WIDTH; j++) {
                line.add(new Boolean(false));
            }
        }

        int w = 0;
        for (String command : commands) {
            if (command.startsWith("rect")) {
                String[] split = command.replace("rect ", "").split("x");
                int x = Integer.parseInt(split[0]);
                int y = Integer.parseInt(split[1]);
                rect(screen, x, y);
            } else if (command.startsWith("rotate row")) {
                String[] split = command.replace("rotate row y=", "").split(" by ");
                int y = Integer.parseInt(split[0]);
                int by = Integer.parseInt(split[1]);
                rotateRow(screen, y, by);
            } else { // rotate column
                String[] split = command.replace("rotate column x=", "").split(" by ");
                int x = Integer.parseInt(split[0]);
                int by = Integer.parseInt(split[1]);
                rotateColumn(screen, x, by);
            }
        }

        int result = 0;
        for (int i = 0; i < screen.size(); i++) {
            LinkedList<Boolean> line = screen.get(i);
            for (int j = 0; j < line.size(); j++) {
                if (line.get(j))
                    result++;
            }
        }

        displayScreen(screen);

        return result;
    }

    private static void rotateRow(LinkedList<LinkedList<Boolean>> screen, int y, int by) {
        LinkedList<Boolean> row = screen.get(y);
        for (int i = 0; i < by; i++) {
            row.addFirst(row.removeLast());
        }
    }

    private static void rotateColumn(LinkedList<LinkedList<Boolean>> screen, int x, int by) {
        for (int j = 0; j < by; j++) {
            boolean val = screen.get(screen.size() - 1).get(x);
            boolean prevVal = false;
            for (int i = 0; i < screen.size(); i++) {
                LinkedList<Boolean> row = screen.get(i);
                prevVal = row.get(x);
                row.set(x, val);
                val = prevVal;
            }
        }
    }

    private static void rect(LinkedList<LinkedList<Boolean>> screen, int x, int y) {
        for (int i = 0; i < y; i++) {
            LinkedList<Boolean> line = screen.get(i);
            for (int j = 0; j < x; j++) {
                line.set(j, true);
            }
        }
    }

    private static void displayScreen(LinkedList<LinkedList<Boolean>> screen) {
        for (int i = 0; i < screen.size(); i++) {
            LinkedList<Boolean> line = screen.get(i);
            for (int j = 0; j < line.size(); j++) {
                System.out.print(line.get(j) ? "#" : ".");
            }
            System.out.print("\n");
        }
    }

}
