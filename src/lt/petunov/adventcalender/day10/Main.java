package lt.petunov.adventcalender.day10;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lt.petunov.adventcalender.utils.Utils;

public class Main {

    class Bot {
        Integer firstChip;
        Integer secondChip;
    }

    HashMap<Integer, Bot> bots = new HashMap<>();
    HashMap<Integer, Integer> outBins = new HashMap<>();


    public static void main(String[] args) {

//        new Main().executeInstructions(Utils.readFile("input/day10/test.txt"));

        new Main().executeInstructions(Utils.readFile("input/day10/input.txt"));
    }

    private void executeInstructions(List<String> strings) {
        // passing chips first
        for (String instruction : strings) {
            if (instruction.startsWith("value")) {
                executeGivesInstruction(instruction);
            }
        }

        while (strings.size() > 0) {
            ArrayList<String> tempStrings = new ArrayList<String>();

            for (String instruction : strings) {
                if (instruction.startsWith("bot ")) {
                    if (!executeBotInstruction(instruction)) {
                        tempStrings.add(instruction);
                    }
                }
            }
            strings = tempStrings;
        }

        int result = outBins.get(0) * outBins.get(1) * outBins.get(2);
        System.out.println("Part two answer is " + result);
    }

    private boolean executeBotInstruction(String instruction) {
        Pattern pattern = Pattern.compile("bot (\\d+) gives low to (\\w+) (\\d+) and high to (\\w+) (\\d+)");
        Matcher matcher = pattern.matcher(instruction);

        matcher.find();

        int sourceBotNumber = Integer.parseInt(matcher.group(1));
        String lowTargetType = matcher.group(2);
        int lowNumber = Integer.parseInt(matcher.group(3));
        String highTargetType = matcher.group(4);
        int highNumber = Integer.parseInt(matcher.group(5));


        Bot sb = bots.get(sourceBotNumber);

        if (sb == null) {
            return false;
        }

        if (sb.firstChip != null && sb.secondChip != null) {
            if (sb.firstChip.equals(17) && sb.secondChip.equals(61) || sb.firstChip.equals(61) && sb.secondChip
                    .equals(17)) {
                System.out.println("Part one answer is " + sourceBotNumber);
            }

            if (sb.firstChip > sb.secondChip) {
                setTarget(highTargetType, highNumber, sb.firstChip);
                setTarget(lowTargetType, lowNumber, sb.secondChip);
            } else {
                setTarget(highTargetType, highNumber, sb.secondChip);
                setTarget(lowTargetType, lowNumber, sb.firstChip);
            }
            sb.firstChip = null;
            sb.secondChip = null;
        } else {
            return false;
        }

        return true;
    }

    private void setTarget(String targetType, int number, Integer value) {
        if (targetType.equals("bot")) {
            setBot(number, value);
        } else {
            outBins.put(number, value);
        }
    }

    private void setBot(int number, Integer value) {
        if (bots.containsKey(number)) {
            Bot bot = bots.get(number);
            if (bot.firstChip == null)
                bot.firstChip = value;
            else if (bot.secondChip == null)
                bot.secondChip = value;
        } else {
            Bot bot = new Bot();
            bot.firstChip = value;
            bots.put(number, bot);
        }
    }

    private void executeGivesInstruction(String instruction) {
        Pattern pattern = Pattern.compile("value (\\d+) goes to bot (\\d+)");
        Matcher matcher = pattern.matcher(instruction);

        matcher.find();
        int val = Integer.parseInt(matcher.group(1));
        int botNumber = Integer.parseInt(matcher.group(2));
        setBot(botNumber, val);
    }
}
