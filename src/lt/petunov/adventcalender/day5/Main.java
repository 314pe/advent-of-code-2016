package lt.petunov.adventcalender.day5;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

public class Main {


    public static void main(String[] args) {
        String doorId = "abc";
        // System.out.println("password for door "+doorId+" is " + getPassword(doorId));
        System.out.println("advanced password for door " + doorId + " is " + getAdvancedPassword(doorId));
        doorId = "uqwqemis";
        // System.out.println("password for door " + doorId + " is " + getPassword(doorId));
        System.out.println("advanced password for door " + doorId + " is " + getAdvancedPassword(doorId));
    }

    private static String getAdvancedPassword(String doorId) {
        char[] result = new char[] { '?', '?', '?', '?', '?', '?', '?', '?' };
        Integer i = 0;

        String md5 = "";
        while (true) {
            String key = doorId + i;
            MessageDigest md;
            try {
                md = MessageDigest.getInstance("MD5");
                md5 = DatatypeConverter.printHexBinary(md.digest(key.getBytes()));
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            i++;

            if (md5.startsWith("00000") && (md5.charAt(5) - 48 < 8) && result[md5.charAt(5) - 48] == '?') {
                result[md5.charAt(5) - 48] = md5.charAt(6);
                if (fullPassword(result))
                    break;
            }
        }

        return String.valueOf(result);
    }

    private static boolean fullPassword(char[] result) {

        for (char c : result)
            if (c == '?')
                return false;

        return true;
    }

    private static String getPassword(String doorId) {
        StringBuffer result = new StringBuffer();
        Integer i = 0;

        String md5 = "";
        while (result.length() < 8) {
            String key = doorId + i;
            MessageDigest md;
            try {
                md = MessageDigest.getInstance("MD5");
                md5 = DatatypeConverter.printHexBinary(md.digest(key.getBytes()));
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            i++;
            if (md5.startsWith("00000")) {
                result.append(md5.substring(5, 6));
            }
        }

        return result.toString();
    }

}
