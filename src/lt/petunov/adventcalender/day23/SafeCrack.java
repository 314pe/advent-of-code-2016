package lt.petunov.adventcalender.day23;

import java.util.HashMap;
import java.util.List;

import lt.petunov.adventcalender.day12.BunnyAssembly;
import lt.petunov.adventcalender.utils.Utils;

public class SafeCrack extends BunnyAssembly {

    private static final String TOGGLE = "tgl";

    public SafeCrack(HashMap<String, Integer> presets) {
        super(presets);
    }

    protected void executeCommand(String command) {
        super.executeCommand(command);
        String[] split = command.split("\\s");

        if (split[0].equals(TOGGLE)) {
            toggle(split[1]);
        }
        // System.out.println(command);
    }

    // Detect multiplication
    private void optimize() {
        if (commands.get(i).equals("cpy b c") && commands.get(i + 1).equals("inc a")
                && commands.get(i + 2).equals("dec c") && commands.get(i + 3).equals("jnz c -2")
                && commands.get(i + 4).equals("dec d") && commands.get(i + 5).equals("jnz d -5")) {
            registers.put("a", registers.get("b") * registers.get("d"));
            i += 6;
        }
    }

    protected void executeCommands(List<String> commands) {
        this.commands = commands;
        i = 0;

        while (i < commands.size()) {
            optimize();
            executeCommand(commands.get(i));
            i++;
        }

        System.out.println("registers " + registers);
    }

    private void toggle(String param) {
        int change = i;
        if (registers.get(param) != null) {
            change += registers.get(param);
        } else {
            change += Integer.parseInt(param);
        }

        if (commands.size() <= change)
            return;

        String command = commands.get(change);
        switch (command.split("\\s")[0]) {
            case COPY:
                command = command.replace(COPY, JUMP_NOT_ZERO);
                break;
            case INCREMENT:
                command = command.replace(INCREMENT, DECREMENT);
                break;
            case DECREMENT:
                command = command.replace(DECREMENT, INCREMENT);
                break;
            case JUMP_NOT_ZERO:
                command = command.replace(JUMP_NOT_ZERO, COPY);
                break;
            case TOGGLE:
                command = command.replace(TOGGLE, INCREMENT);
                break;
        }
        commands.set(change, command);
    }

    public static void main(String[] args) {
        System.out.println("Part 1");
        HashMap<String, Integer> registers = new HashMap<>();
        registers.put("a", 7);
        new SafeCrack(registers).executeCommands(Utils.readFile("input/day23/input.txt"));

        System.out.println("Part 2");
        registers.put("a", 12);
        new SafeCrack(registers).executeCommands(Utils.readFile("input/day23/input.txt"));
    }

}
