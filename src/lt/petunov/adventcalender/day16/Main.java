package lt.petunov.adventcalender.day16;

public class Main {

    private int length = 0;
    private String input = "";

    public Main(int length, String input) {
        this.length = length;
        this.input = input;
    }

    public static void main(String[] args) {
        // Test input
        // System.out.println("Checksum " + new Main(20, "10000").calculateCheckSum());
        // Part 1
        // System.out.println("Checksum " + new Main(272, "10111011111001111").calculateCheckSum());
        // Part 2
        System.out.println("Checksum " + new Main(35651584, "10111011111001111").calculateCheckSum());
    }

    private String calculateCheckSum() {
        if (input.length() < length)
            dragonCurve();

        do {
            String[] split = input.split("");
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < split.length; i += 2) {
                if (split[i].equals(split[i + 1]))
                    sb.append("1");
                else
                    sb.append("0");
            }
            input = sb.toString();
        } while (input.length() % 2 == 0);

        return input;
    }

    private void dragonCurve() {
        while (input.length() < length) {
            StringBuilder sb = new StringBuilder();

            input = input + "0" + getNegation(sb.append(input).reverse().toString());
        }

        input = input.substring(0, length);
    }

    private String getNegation(String input) {
        StringBuilder sb = new StringBuilder();

        for (String s : input.split("")) {
            sb.append(s.equals("1") ? "0" : "1");
        }

        return sb.toString();
    }

}
