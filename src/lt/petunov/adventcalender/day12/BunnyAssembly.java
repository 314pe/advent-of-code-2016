package lt.petunov.adventcalender.day12;

import java.util.HashMap;
import java.util.List;

import lt.petunov.adventcalender.utils.Utils;

public class BunnyAssembly {

    protected static final String COPY = "cpy";
    protected static final String INCREMENT = "inc";
    protected static final String DECREMENT = "dec";
    protected static final String JUMP_NOT_ZERO = "jnz";

    protected HashMap<String, Integer> registers = new HashMap<>();
    protected Integer i = 0;
    protected List<String> commands;

    public BunnyAssembly() {
        registers.put("a", 0);
        registers.put("b", 0);
        registers.put("c", 0);
        registers.put("d", 0);
    }

    public BunnyAssembly(HashMap<String, Integer> presets) {
        this();
        registers.putAll(presets);
    }

    protected void executeCommands(List<String> commands) {
        this.commands = commands;
        i = 0;

        while (i < commands.size()) {
            executeCommand(commands.get(i));
            i++;
        }

        System.out.println("registers " + registers);
    }

    protected void executeCommand(String command) {
        String[] split = command.split("\\s");

        switch (split[0]) {
            case COPY:
                copy(split[1], split[2]);
                break;
            case INCREMENT:
                increment(split[1]);
                break;
            case DECREMENT:
                decrement(split[1]);
                break;
            case JUMP_NOT_ZERO:
                jumpNotZero(split[1], split[2]);
                break;
        }
    }

    private void jumpNotZero(String register, String command) {
        int idx = 0;
        if (registers.containsKey(command))
            idx = registers.get(command);
        else
            idx = Integer.parseInt(command);

        if ((registers.containsKey(register) && registers.get(register) != 0) || (!registers.containsKey(register) && Integer
                .parseInt(register) != 0))
            i += idx - 1;
    }

    private void decrement(String register) {
        registers.put(register, registers.get(register) - 1);
    }

    private void increment(String register) {
        registers.put(register, registers.get(register) + 1);
    }

    private void copy(String source, String target) {
        if (registers.get(source) != null) {
            registers.put(target, registers.get(source));
        } else {
            registers.put(target, Integer.parseInt(source));
        }
    }

    public static void main(String[] args) {
        List<String> lines = Utils.readFile("input/day12/input.txt");

        // Part 1
        System.out.println("Part 1");
        new BunnyAssembly().executeCommands(lines);

        // Part 2
        System.out.println("Part 2");
        HashMap<String, Integer> registers = new HashMap<>();
        registers.put("c", 1);
        new BunnyAssembly(registers).executeCommands(lines);

    }

}
