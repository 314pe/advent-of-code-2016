package lt.petunov.adventcalender.day7;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lt.petunov.adventcalender.utils.Utils;

public class Main {

    public static void main(String[] args) {
        // Part 1
        System.out.println("Count tls supported ips... " + countTLSIPs(Utils.readFile("input/day7/input.txt")));
        // Part 2
        System.out.println("Count ssl supported ips... " + countSSLIPs(Utils.readFile("input/day7/input.txt")));
    }

    private static int countSSLIPs(List<String> ips) {
        int result = 0;

        for (String ip : ips) {
            if (supportsSSL(ip))
                result++;
        }

        return result;
    }

    private static boolean supportsSSL(String ip) {
        // Remove hypernets from ip
        String[] supernet = ip.split("\\[[a-z]*\\]");
        // Remove supernets from ip
        String[] hypernet = ip.split("([a-z]*\\[)|(\\][a-z]*\\[)|(\\][a-z]*)");

        for (String sup : supernet) {
            // May contain more than one aba
            for (String aba : getAbas(sup)) {
                // Is it a valid aba?
                if (aba != null && aba.charAt(0) != aba.charAt(1)) {
                    for (String hyper : hypernet) {
                        if (hyper.contains(getBab(aba))) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }


    private static CharSequence getBab(String aba) {
        return "" + aba.charAt(1) + aba.charAt(0) + aba.charAt(1);
    }

    private static List<String> getAbas(String address) {
        List<String> result = new ArrayList<>();

        // Abas may overlap
        Pattern pattern = Pattern.compile("(?=((\\w)(\\w)\\2))");
        Matcher matcher = pattern.matcher(address);

        while (matcher.find())
            result.add(matcher.group(1));

        return result;
    }

    private static int countTLSIPs(List<String> ips) {
        int result = 0;

        for (String ip : ips) {
            if (supportsTLS(ip))
                result++;
        }

        return result;
    }

    private static boolean supportsTLS(String ip) {
        String[] split = ip.split("\\[|\\]");

        boolean validAddress = false;
        for (int i = 0; i < split.length; i++) {
            if (containsTLSPattern(split[i])) {
                // hypernet part
                if (i % 2 != 0) {
                    return false;
                    // supernet part
                } else {
                    validAddress = true;
                }
            }
        }

        return validAddress;
    }

    // Find abba
    private static boolean containsTLSPattern(String string) {
        Pattern pattern = Pattern.compile("((\\w)(\\w)\\3\\2)");
        Matcher matcher = pattern.matcher(string);

        return matcher.find() && matcher.group(1).charAt(0) != matcher.group(1).charAt(1);
    }

}
