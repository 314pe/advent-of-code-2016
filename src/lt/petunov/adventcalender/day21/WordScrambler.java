package lt.petunov.adventcalender.day21;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lt.petunov.adventcalender.utils.Utils;

public class WordScrambler {

    private String word;

    private List<String> commands;

    private static final String SWAP_POSITION = "swap position";
    private static final String SWAP_LETTERS = "swap letter";
    private static final String ROTATE_BASED_ON_POSITION = "rotate based on position of letter";
    private static final String ROTATE_RIGHT_LEFT = "rotate";
    private static final String REVERSE_POSITIONS = "reverse positions";
    private static final String MOVE_POSITION = "move position";
    private static final String RIGHT = "right";

    public WordScrambler(List<String> commands) {
        this.commands = commands;
    }

    public static void main(String[] args) {
        List<String> input = Utils.readFile("input/day21/input.txt");

        WordScrambler scrambler = new WordScrambler(input);

        System.out.println("Part 1 " + scrambler.scramble("abcdefgh"));

        System.out.println("Part 2 " + scrambler.unscramble("fbgdceah"));
    }

    private String unscramble(String word) {
        for (String perm : getAllPermutations(word)) {
            if (scramble(perm).equals(word)) {
                return perm;
            }
        }

        return null;
    }

    private Set<String> getAllPermutations(String word) {
        HashSet<String> result = new HashSet<>();

        if (word == null) {
            return result;
        }
        if (word.length() == 0) {
            result.add("");
            return result;
        }

        char start = word.charAt(0);
        String rem = word.substring(1);
        Set<String> words = getAllPermutations(rem);
        for (String perms : words) {
            for (int i = 0; i <= perms.length(); i++) {
                result.add(charAdd(perms, start, i));
            }
        }

        return result;
    }

    private String charAdd(String str, char c, int i) {
        String first = str.substring(0, i);
        String last = str.substring(i);
        return first + c + last;
    }

    private String scramble(String word) {
        this.word = word;

        for (String command : commands) {
            if (command.startsWith(SWAP_POSITION)) {
                swapPosition(command);
            } else if (command.startsWith(SWAP_LETTERS)) {
                swapLetters(command);
            } else if (command.startsWith(ROTATE_BASED_ON_POSITION)) {
                rotatePosition(command);
            } else if (command.startsWith(ROTATE_RIGHT_LEFT)) {
                rotate(command);
            } else if (command.startsWith(REVERSE_POSITIONS)) {
                reversePositions(command);
            } else if (command.startsWith(MOVE_POSITION)) {
                movePosition(command);
            }
        }

        return this.word;
    }

    private void rotatePosition(String command) {
        Pattern pattern = Pattern.compile(ROTATE_BASED_ON_POSITION + " (\\w)");
        Matcher matcher = pattern.matcher(command);

        matcher.find();
        int x = word.indexOf(matcher.group(1));

        if (x >= 4)
            x++;

        rotateRight(++x);
    }

    private void rotateRight(int i) {
        i = i % word.length();
        String end = word.substring(word.length() - i);
        String start = word.substring(0, word.length() - i);

        word = end + start;
    }

    private void movePosition(String command) {
        Pattern pattern = Pattern.compile(MOVE_POSITION + " (\\d+) to position (\\d+)");
        Matcher matcher = pattern.matcher(command);

        matcher.find();
        Integer x = Integer.parseInt(matcher.group(1));
        Integer y = Integer.parseInt(matcher.group(2));

        char letter = word.charAt(x);

        word = word.substring(0, x) + word.substring(x + 1);
        y = y % (word.length() + 1);
        word = word.substring(0, y) + letter + word.substring(y);
    }

    private void rotate(String command) {
        Pattern pattern = Pattern.compile(ROTATE_RIGHT_LEFT + " (\\w+) (\\d+) step");
        Matcher matcher = pattern.matcher(command);

        matcher.find();
        String direction = matcher.group(1);
        Integer x = Integer.parseInt(matcher.group(2));

        String start = null;
        String end = null;
        if (direction.equals(RIGHT)) {
            end = word.substring(word.length() - x);
            start = word.substring(0, word.length() - x);
        } else {
            end = word.substring(x);
            start = word.substring(0, x);
        }
        word = end + start;
    }

    private void reversePositions(String command) {
        Pattern pattern = Pattern.compile(REVERSE_POSITIONS + " (\\d+) through (\\d+)");
        Matcher matcher = pattern.matcher(command);

        matcher.find();
        Integer x = Integer.parseInt(matcher.group(1));
        Integer y = Integer.parseInt(matcher.group(2));

        String start = "";
        String reverse = "";
        String end = "";

        if (x > 0)
            start = word.substring(0, x);

        if (y < word.length() - 1)
            end = word.substring(y+1);

        reverse = new StringBuilder(word.substring(x, y + 1)).reverse().toString();

        word = start + reverse + end;
    }

    private void swapLetters(String command) {
        Pattern pattern = Pattern.compile(SWAP_LETTERS + " (\\w) with letter (\\w)");
        Matcher matcher = pattern.matcher(command);

        matcher.find();
        char x = matcher.group(1).charAt(0);
        char y = matcher.group(2).charAt(0);

        word = word.replace(x, '?');
        word = word.replace(y, x);
        word = word.replace('?', y);
    }

    private void swapPosition(String command) {
        Pattern pattern = Pattern.compile(SWAP_POSITION + " (\\d+) with position (\\d+)");
        Matcher matcher = pattern.matcher(command);

        matcher.find();
        Integer x = Integer.parseInt(matcher.group(1));
        Integer y = Integer.parseInt(matcher.group(2));

        char[] array = word.toCharArray();
        char temp = array[x];
        array[x] = array[y];
        array[y] = temp;

        word = new String(array);
    }

}
